package com.example.demoapi.mappers;

import java.util.stream.Collectors;

import com.example.demoapi.models.dtos.PersonDTO;
import com.example.demoapi.models.entities.Person;
import com.example.demoapi.models.forms.PersonForm;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PersonMapper implements BaseMapper<PersonDTO, PersonForm, Person> {

    private final ProductMapper productMapper;

    public PersonMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    public Person formToEntity(PersonForm form)
    {
        Person p = new Person();
        p.setFirstname(form.getFirstname());
        p.setLastname(form.getLastname());

        return p;
    }

    public PersonDTO toDto(Person person)
    {
        if(person != null && person.getPersonId() > 0)
        {
            return PersonDTO.builder()
                .id(person.getPersonId())
                .lastname(person.getLastname())
                .firstname(person.getFirstname())
                .basket(person.getBasket() != null ? 
                    person.getBasket().stream()
                        .map(this.productMapper::toDto)
                        .collect(Collectors.toList())
                         : null)
                .build();
        }
        return null;
    }

    public Person dtoToEntity(PersonDTO dto)
    {
        Person p = new Person();

        if(dto != null && dto.getId() > 0)
        {
            p.setPersonId(dto.getId());
            p.setFirstname(dto.getFirstname());
            p.setLastname(dto.getLastname());
        }
        return p;
    }
}

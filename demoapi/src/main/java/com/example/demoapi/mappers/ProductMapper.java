package com.example.demoapi.mappers;

import com.example.demoapi.models.dtos.ProductDTO;
import com.example.demoapi.models.entities.Product;
import com.example.demoapi.models.forms.ProductForm;

import org.springframework.stereotype.Service;

@Service
public class ProductMapper implements BaseMapper<ProductDTO, ProductForm, Product>
{

    @Override
    public Product formToEntity(ProductForm form) {
        Product p = new Product();
        
        p.setCategory(form.getCategory());
        p.setName(form.getName());
        p.setPrice(form.getPrice());

        return p;
    }

    @Override
    public ProductDTO toDto(Product entity) {
        if(entity != null && entity.getProductId() > 0)
        {
            return ProductDTO.builder()
                .id(entity.getProductId())
                .category(entity.getCategory())
                .name(entity.getName())
                .price(entity.getPrice())
                .build();
        }
        return null;
    }

    @Override
    public Product dtoToEntity(ProductDTO dto) {
        Product p = new Product();

        if(dto != null && dto.getId() > 0)
        {
            p.setProductId(dto.getId());
            p.setCategory(dto.getCategory());
            p.setName(dto.getName());
            p.setPrice(dto.getPrice());
        }
        
        return p;
    }
    
}
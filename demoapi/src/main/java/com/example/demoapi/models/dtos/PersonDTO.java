package com.example.demoapi.models.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonDTO {
    long id;
    String lastname;
    String firstname;
    List<ProductDTO> basket;
}

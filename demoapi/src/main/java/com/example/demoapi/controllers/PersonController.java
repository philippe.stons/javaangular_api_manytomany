package com.example.demoapi.controllers;

import java.util.List;

import javax.validation.Valid;

import com.example.demoapi.models.dtos.PersonDTO;
import com.example.demoapi.models.forms.PersonForm;
import com.example.demoapi.services.impl.PersonServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/person")
public class PersonController {
    private final PersonServiceImpl personServiceImpl;

    public PersonController(PersonServiceImpl personServiceImpl) {
        this.personServiceImpl = personServiceImpl;
    }

    @GetMapping
    public ResponseEntity<List<PersonDTO>> getList()
    {
        return ResponseEntity.ok(this.personServiceImpl.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> getOneById(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.personServiceImpl.getOneById(id));
    }

    @PostMapping()
    public ResponseEntity<Object> insert(@Valid @RequestBody PersonForm form) throws Exception
    {
        return ResponseEntity.ok(this.personServiceImpl.insert(form));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<PersonDTO> update(@Valid @RequestBody PersonForm form, 
        @PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.personServiceImpl.update(form, id));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> delete(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.personServiceImpl.delete(id));
    }

    @PutMapping(path = "{id}/add/{productId}")
    public ResponseEntity<PersonDTO> addToBasket(@PathVariable(name = "id") Long id, @PathVariable(name = "productId") Long productId)
    {

        return ResponseEntity.ok(this.personServiceImpl.addProductToBasket(id, productId));
    }
}

package com.example.demoapi.controllers;

import java.util.List;

import javax.validation.Valid;

import com.example.demoapi.models.dtos.ProductDTO;
import com.example.demoapi.models.forms.ProductForm;
import com.example.demoapi.services.impl.ProductServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class ProductController {
    private final ProductServiceImpl ProductServiceImpl;

    public ProductController(ProductServiceImpl productServiceImpl) {
        this.ProductServiceImpl = productServiceImpl;
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> getList()
    {
        return ResponseEntity.ok(this.ProductServiceImpl.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getOneById(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.ProductServiceImpl.getOneById(id));
    }

    @PostMapping()
    public ResponseEntity<ProductDTO> insert(@Valid @RequestBody ProductForm form) throws Exception
    {
        return ResponseEntity.ok(this.ProductServiceImpl.insert(form));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<ProductDTO> update(@Valid @RequestBody ProductForm form, 
        @PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.ProductServiceImpl.update(form, id));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> delete(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.ProductServiceImpl.delete(id));
    }
}

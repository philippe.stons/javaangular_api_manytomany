package com.example.demoapi.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.example.demoapi.mappers.PersonMapper;
import com.example.demoapi.models.dtos.PersonDTO;
import com.example.demoapi.models.entities.Person;
import com.example.demoapi.models.entities.Product;
import com.example.demoapi.models.forms.PersonForm;
import com.example.demoapi.repositories.PersonRepository;
import com.example.demoapi.repositories.ProductRepository;
import com.example.demoapi.services.BaseService;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PersonServiceImpl implements BaseService<PersonDTO, PersonForm, Long> {
    private final PersonRepository personRepository;
    private final PersonMapper personMapper;
    private final ProductRepository productRepository;

    public PersonServiceImpl(PersonRepository personRepository, PersonMapper personMapper,
    ProductRepository productRepository) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
        this.productRepository = productRepository;
    }

    public List<PersonDTO> getAll()
    {
        return this.personRepository.findAll()
            .stream()
            .map(this.personMapper::toDto)
            .collect(Collectors.toList());
    }

    public PersonDTO getOneById(Long id)
    {
        return this.personMapper.toDto(this.personRepository.findById(id).orElse(null));
    }

    public PersonDTO insert(PersonForm form)
    {
        Person p = this.personMapper.formToEntity(form);
        return this.personMapper.toDto(this.personRepository.save(p));
    }

    @Override
    public String delete(Long id) {
        Person p = this.personRepository.findById(id).orElse(null);
        
        this.personRepository.delete(p);

        return String.format("Deleted Person with id : %d", id);
    }

    @Override
    public PersonDTO update(PersonForm form, Long id) {
        Person p = this.personRepository.findById(id).orElse(null);
        
        p.setFirstname(form.getFirstname());
        p.setLastname(form.getLastname());
        this.personRepository.save(p);

        return this.personMapper.toDto(p);
    }

    public PersonDTO addProductToBasket(Long id, Long productId)
    {
        Product pr = this.productRepository.findById(productId).orElse(null);
        Person p = this.personRepository.findById(id).orElse(null);

        if(pr != null && p != null)
        {
            p.getBasket().add(pr);
            this.personRepository.save(p);
        }

        return this.personMapper.toDto(p);
    }
}

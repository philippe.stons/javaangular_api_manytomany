package com.example.demoapi.services.impl;


import java.util.List;
import java.util.stream.Collectors;

import com.example.demoapi.mappers.ProductMapper;
import com.example.demoapi.models.dtos.ProductDTO;
import com.example.demoapi.models.entities.Product;
import com.example.demoapi.models.forms.ProductForm;
import com.example.demoapi.repositories.ProductRepository;
import com.example.demoapi.services.BaseService;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductServiceImpl implements BaseService<ProductDTO, ProductForm, Long> {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public List<ProductDTO> getAll()
    {
        return this.productRepository.findAll()
                    .stream()
                    .map(this.productMapper::toDto)
                    .collect(Collectors.toList());
    }

    public ProductDTO getOneById(Long id)
    {
        return this.productMapper.toDto(this.productRepository.findById(id).orElse(null));
    }

    public ProductDTO insert(ProductForm form)
    {
        Product p = this.productMapper.formToEntity(form);
        return this.productMapper.toDto(this.productRepository.save(p));
    }

    @Override
    public String delete(Long id) {
        Product p = this.productRepository.findById(id).orElse(null);
        
        this.productRepository.delete(p);

        return String.format("Deleted product with id : %d", id);
    }

    @Override
    public ProductDTO update(ProductForm form, Long id) {
        Product p = this.productRepository.findById(id).orElse(null);
        
        p.setCategory(form.getCategory());
        p.setName(form.getName());
        p.setPrice(form.getPrice());
        this.productRepository.save(p);

        return this.productMapper.toDto(p);
    }
}

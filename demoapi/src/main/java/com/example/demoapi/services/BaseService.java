package com.example.demoapi.services;

import java.util.List;

public interface BaseService<TDTO, TFORM, TID> {
    List<TDTO> getAll();
    TDTO getOneById(TID id);
    TDTO insert(TFORM form);
    String delete(TID id);
    TDTO update(TFORM form, TID id);
}
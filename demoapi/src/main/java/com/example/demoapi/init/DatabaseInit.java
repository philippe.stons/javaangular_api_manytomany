package com.example.demoapi.init;

import java.util.Arrays;
import java.util.List;

import com.example.demoapi.models.entities.Person;
import com.example.demoapi.models.entities.Product;
import com.example.demoapi.repositories.PersonRepository;
import com.example.demoapi.repositories.ProductRepository;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DatabaseInit implements InitializingBean {
    private final PersonRepository personRepository;
    private final ProductRepository productRepository;

    public DatabaseInit(PersonRepository personRepository, ProductRepository productRepository)
    {
        this.personRepository = personRepository;
        this.productRepository = productRepository;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info(">>> !! INITIALIZE DATABASE !! <<<");

        List<Person> persons = Arrays.asList(
            Person.builder()
                .firstname("Johann Sebastian")
                .lastname("Bach")
                .build(),
            Person.builder()
                .firstname("Riri")
                .lastname("Duck")
                .build(),
            Person.builder()
                .firstname("Fifi")
                .lastname("Duck")
                .build(),
            Person.builder()
                .firstname("Loulou")
                .lastname("Duck")
                .build()
        );

        List<Product> products = Arrays.asList(
            Product.builder()
                .category("C")
                .name("product c")
                .price(23.5)
                .build(),
            Product.builder()
                .category("A")
                .name("product A")
                .price(51.5)
                .build(),
            Product.builder()
                .category("B")
                .name("product b")
                .price(37.5)
                .build()
        );
        products.forEach(this.productRepository::save);

        persons.get(0).setBasket(products);

        persons.forEach(this.personRepository::save);


    }
    
}
